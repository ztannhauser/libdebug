
#ifndef DEBUG
	#define DEBUG 0
#endif

#include <stdio.h>
#include <stdlib.h>

#if DEBUG

#define ENTER                                 \
	verprintf("<%s>\n", __PRETTY_FUNCTION__); \
	stackdepth++;
#define EXIT      \
	stackdepth--; \
	verprintf("</%s>\n", __PRETTY_FUNCTION__);

#define getforstr(val)               \
	(_Generic(val, signed char       \
			  : "%%sc: %i", unsigned char  \
			  : "%%uc: %u", signed short   \
			  : "%%ss: %i", unsigned short \
			  : "%%us: %u", signed int     \
			  : "%%si: %i", unsigned int   \
			  : "%%ui: %u", signed long    \
			  : "%%sl: %li", unsigned long \
			  : "%%ul: %lu", float         \
			  : "%%f: %f", double         \
			  : "%%d: %f", long double    \
			  : "%%ld: %f", default        \
			  : "%%p: %p"))
		
#define verpv(val)                   \
	({ verprintf(#val " == ");          \
	printf(getforstr(val), val); \
	printf("\n"); })

#define verpvb(val) verprintf(#val " == \"%s\"\n", (val) ? "true" : "false")
#define verpvs(val) verprintf(#val " == \"%s\"\n", val)
#define verpvc(val) verprintf(#val " == '%c'\n", val)
#define verpvsn(ptr, len) verprintf(#ptr " == \"%.*s\"\n", len, ptr)
#define verpvfs(format_string, val) \
	verprintf(#val " == %s: " format_string "\n", format_string, val);

#define NOPE  todo_function(__FILE__, __LINE__, "NOPE");
#define CHECK todo_function(__FILE__, __LINE__, "CHECK");
#define TODO  todo_function(__FILE__, __LINE__, "TODO");

#define HERE verprintf("%s: line %u\n", __FILE__, __LINE__);

extern char *verbose_prefix;
extern size_t stackdepth;

void verprintf(const char *format, ...) __attribute__((format(printf, 1, 2)));

void todo_function(const char* file, int line, const char* op);

#else

#define ENTER
#define EXIT
#define verpv(val) ;
#define verpvb(val) ;
#define verpvs(val) ;
#define verpvc(val) ;
#define verpvsn(val, val2) ;
#define verprintf(...)
#define verpvfs(format_string, val)
#define NOPE abort();
#define HERE
#define TODO abort()
#define CHECK abort()

#endif

