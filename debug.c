#include <stdbool.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <unistd.h>

#ifndef DEBUG
	#define DEBUG 0
#endif

#if DEBUG

size_t stackdepth = 0;
char *verbose_prefix = "";

void verprintf(const char *format, ...) {
	printf("%s", verbose_prefix);
	size_t i;
	for(i = 0; i < stackdepth; i++) {
		printf("    ");
	}
	va_list arg;
	va_start(arg, format);
	vprintf(format, arg);
	va_end(arg);
}

void todo_function(const char* file, int line, const char* op)
{
	verprintf("%s: line %u: %s\n", file, line, op);
	char line_buffer[30];
	sprintf(line_buffer, "+%i", line);
	const char* args[] = {"gedit", file, line_buffer, NULL};
	execvp(args[0], args);
}

#endif

















